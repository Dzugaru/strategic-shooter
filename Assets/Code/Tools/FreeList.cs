﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FreeList
{
    IFreeListObject root;
    Func<object> construct;

    public FreeList(Func<object> construct)
    {
        this.construct = construct;
    }

    public T New<T>() where T : IFreeListObject
    {
        if (root == null)
        {
            //Debug.Log("Constructed new " + typeof(T).Name);
            return (T)construct();
        }
        else
        {            
            var obj = (T)root;
            root = root.freeListNext;
            obj.InitializeForFreeList();
            return obj;            
        }
    }

    public void Return(IFreeListObject obj)
    {
        obj.freeListNext = root;
        root = obj;        
    }
}

public interface IFreeListObject
{
    IFreeListObject freeListNext { get; set; }
    void InitializeForFreeList();
}

