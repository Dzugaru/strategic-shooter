﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using RNG = UnityEngine.Random;

public class Weapon : MonoBehaviour
{
    public enum FireMode
    {
        SemiAuto,
        Auto
    }

    public float rof;
    public float damage;
    public int magazineSize;
    public float reloadTime;
    public float accuracy;
    public float bulletSpeed;
    public FireMode fireMode;
    public Bullet bulletPrefab;
    public Transform muzzle;
    
    Transform bulletContainer;
    
    int roundsLeft;
    float lastFireTime = float.MinValue;
    int firesSinceTriggerHeld;

    bool isReloading;
    float reloadStartTime;

    public bool IsTriggerHeld { get; set; }

    void Awake()
    {
        roundsLeft = magazineSize;
        bulletContainer = SceneManager.GetActiveScene().GetRootGameObjects().First(x => x.name == "BulletContainer").transform;
    }

    void Start()
    {
        
    }
    
    void FixedUpdate()
    {
        if (isReloading)
            UpdateReload();
        else
            UpdateFiring();
    }    

    void UpdateReload()
    {
        if(Time.time - reloadStartTime > reloadTime)
        {
            roundsLeft = magazineSize;
            isReloading = false;
        }
    }

    void UpdateFiring()
    {
        if(!IsTriggerHeld)
        {
            firesSinceTriggerHeld = 0;
        }
        else
        {
            bool isOffCooldown = Time.time - lastFireTime > 1 / rof;
            bool fireAllowed = fireMode == FireMode.Auto || firesSinceTriggerHeld == 0;
            if (isOffCooldown && fireAllowed)
            {                
                if (firesSinceTriggerHeld == 0)
                    lastFireTime = Time.time;
                else
                    lastFireTime += 1 / rof;
                firesSinceTriggerHeld++;
                Fire();                    
                UpdateFiring(); //allows firing more than one round in a tick
            }
            
        }
    }

    public void Reload()
    {
        isReloading = true;
    }

    void Fire()
    {
        var angularDrift = RNG.Range(0, Mathf.PI);
        var radialDrift = Tools.GaussRandom() * accuracy * Mathf.Deg2Rad;

        Vector3 driftDir = Mathf.Cos(angularDrift) * muzzle.right + Mathf.Sin(angularDrift) * muzzle.up;
        Vector3 dir = muzzle.forward + radialDrift * driftDir;

        var bullet = Instantiate<Bullet>(bulletPrefab);
        bullet.transform.SetParent(bulletContainer);
        bullet.transform.position = muzzle.position;
        bullet.transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
        bullet.speed = bulletSpeed;
        bullet.damage = damage;
    }
}
