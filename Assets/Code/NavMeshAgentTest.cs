﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class NavMeshAgentTest : MonoBehaviour
{
    List<Vector3> corners = new List<Vector3>();

    NavMeshPath path;
    public Transform goal;

    void Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
        agent.CalculatePath(goal.position, path);
        agent.path = path;

        corners = path.corners.ToList();
    }

    
    void Update()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.magenta;
        //foreach(var c in corners)
        //    Gizmos.DrawCube(c, new Vector3(0.1f, 0.1f, 0.1f));


        RaycastHit hit;
        if(Physics.Raycast(new Ray(goal.position, transform.position - goal.position), out hit))
        {
            Gizmos.color = Color.magenta;        
            Gizmos.DrawLine(goal.position, hit.point);
        }
    }
}
