﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

class Player : MonoBehaviour
{
    int wplaneMask;
    NavMeshAgent navMeshAgent;

    public Weapon weapon;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false;
        wplaneMask = LayerMask.GetMask("WalkablePlane");
    }

    void Update()
    {
        Vector2 movementInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if(movementInput.x != 0 || movementInput.y != 0)
        {
            float amount = Mathf.Min(1f, movementInput.magnitude);
            movementInput = amount * movementInput.normalized;
            Vector2 motion = navMeshAgent.speed * Time.deltaTime * movementInput;
            Vector3 motion3d = new Vector3(motion.x, 0, motion.y);
            navMeshAgent.Move(motion3d);
            //navMeshAgent.SetDestination(transform.position + motion3d);
        }

        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(mouseRay, out hit, 100f, wplaneMask))
        {
            //Debug.Log(hit.collider.gameObject.name);
            Vector2 aimDir = (hit.point - transform.position).ProjectOnGround();
            Quaternion aimRot = Quaternion.LookRotation(new Vector3(aimDir.x, 0, aimDir.y), Vector3.up);
            transform.rotation = aimRot;
        }

        weapon.IsTriggerHeld = Input.GetAxis("Fire1") > 0;
    }
}

