﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public float maxDist = 1000;
    public GameObject hitPrefab;
    public float damage;

    bool isHit;
    float distTraveled = 0;
    
   
    void Start()
    {
        
    }

    void Update()
    {
        if(isHit)
        {
            Destroy(gameObject);
            return;
        }

        float predictedDist = Time.deltaTime * speed;
        var (collided, dist) = UpdateCollision(predictedDist);           

        distTraveled += dist;
        if (distTraveled > maxDist)
            Destroy(gameObject);
        else
            transform.position += dist * transform.forward;
    }

    (bool, float) UpdateCollision(float dist)
    {
        RaycastHit hit;
        if(!Physics.Raycast(transform.position, transform.forward, out hit, dist))        
            return (false, dist);

        Hit(hit);
        return (true, hit.distance);
    }

    void Hit(RaycastHit hit)
    {
        //Effect
        var hitEffect = Instantiate(hitPrefab);
        hitEffect.GetComponent<ParticleSystem>().Play();
        hitEffect.transform.position = hit.point;
        hitEffect.transform.rotation = Quaternion.LookRotation(hit.normal, Vector3.up);

        //Damage
        IDamageable damageable = hit.collider.GetComponentInParent<IDamageable>();        
        if (damageable != null)
            damageable.Damage(damage); //TODO: modify on distance traveled? headshot?

        //We need to draw a trail for one last frame
        isHit = true;
    }
}
