﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RNG = UnityEngine.Random;

public static class Tools
{
    public static string PrettyPrint<T>(this IEnumerable<T> list)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var x in list)
            sb.Append(x.ToString() + " ");
        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }
    public static T MaxBy<T, U>(this IEnumerable<T> source, Func<T, U> selector) where U : IComparable<U>
    {
        if (source == null) throw new ArgumentNullException("source");
        bool first = true;
        T maxObj = default(T);
        U maxKey = default(U);
        foreach (var item in source)
        {
            if (first)
            {
                maxObj = item;
                maxKey = selector(maxObj);
                first = false;
            }
            else
            {
                U currentKey = selector(item);
                if (currentKey.CompareTo(maxKey) > 0)
                {
                    maxKey = currentKey;
                    maxObj = item;
                }
            }
        }
        if (first) throw new InvalidOperationException("Sequence is empty.");
        return maxObj;
    }
    public static Vector2 OrthogonalRight(this Vector2 v)
    {
        return new Vector2(v.y, -v.x);
    }
    public static Vector2 ProjectOnGround(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    //NOTE: box-muller, won't generate anything abs(x) > 4.7
    static float? cachedGaussRandom;
    public static float GaussRandom()
    {
        if(cachedGaussRandom.HasValue)
        {
            float x = cachedGaussRandom.Value;
            cachedGaussRandom = null;
            return x;
        }

        float u, v, s;

        do
        {
            u = 2 * RNG.value - 1f;
            v = 2 * RNG.value - 1f;
            s = u * u + v * v;
        }
        while (s == 0 || s >= 1f);

        float fac = Mathf.Sqrt(-2 * Mathf.Log(s) / s);
        cachedGaussRandom = v * fac;
        return u * fac;
    }
}

