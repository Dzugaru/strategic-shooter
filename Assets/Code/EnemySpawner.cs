﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    public float radius;
    public float rate;
    public int amount;
    public GameObject prefab;

    float lastTime = float.MinValue;    
    
    void Update()
    {
        if (amount == 0)
            return;

        if(Time.time - lastTime > 1 / rate)
        {
            if (lastTime == float.MinValue)
                lastTime = Time.time;
            else
                lastTime += 1 / rate;

            Vector2 d = Random.insideUnitCircle * radius;
            Vector3 pos = transform.position + new Vector3(d.x, 0, d.y);
            NavMeshHit hit;
            if(NavMesh.SamplePosition(pos, out hit, radius, NavMesh.AllAreas))
            {
                var go = Instantiate(prefab);
                go.transform.position = hit.position;
                amount--;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
