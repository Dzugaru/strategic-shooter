﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour
{
    public GameObject player;
    public AnimationCurve corrCurve;
    public float corrCurveSpan, corrSpeed;

    [Range(0, 1)]
    public float speedSmoothing;

    public float speedCorrection;

    const float targetSpeedUpdateTime = 0.1f;
    float targetLastTime = float.MinValue;
    Vector3? targetLastPosition;
    Vector3 targetSmoothedSpeed;

    void Start()
    {
        
    }
    
    void Update()
    {        
        //UpdateTargetSpeed();

        Vector3 target = player.transform.position;

        //NOTE: doesn't work well
        //target += targetSmoothedSpeed * speedCorrection;

        Vector3 diff = target - transform.position;
        float correctionSpeed = corrSpeed * corrCurve.Evaluate(Mathf.Clamp(diff.magnitude / corrCurveSpan, 0, 1));        

        Vector3 correction = Time.deltaTime * correctionSpeed * diff.normalized;
        if (correction.magnitude > diff.magnitude)
            transform.position = target;
        else
            transform.position += correction;
    }

    private void UpdateTargetSpeed()
    {
        if (Time.time - targetLastTime > targetSpeedUpdateTime)
        {
            if (targetLastPosition.HasValue)
            {
                Vector3 instantTargetSpeed = (player.transform.position - targetLastPosition.Value) / targetSpeedUpdateTime;
                targetSmoothedSpeed = Vector3.Lerp(instantTargetSpeed, targetSmoothedSpeed, speedSmoothing);                                
            }

            targetLastPosition = player.transform.position;
            targetLastTime = Time.time;
        }
    }
}
