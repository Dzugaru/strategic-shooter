﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI
{
    public abstract class AIBehavior : IFreeListObject
    {
        public IAgent agent;
        protected AIBehaviorFactory factory;

        public AIBehaviorStatus status;

        public IFreeListObject freeListNext { get; set; }

        public abstract bool Do();
        public abstract void InitializeForFreeList();
        public virtual AIBehavior Init(AIBehaviorFactory factory, IBehaviorSettings settings)
        {
            this.factory = factory;            
            return this;
        }

        public void Free()
        {
            this.factory.Return(this);
        }
    }

    public enum AIBehaviorStatusType
    {
        Default,
        Running, 
        Success,
        Failed
    }

    public struct AIBehaviorStatus
    {
        public AIBehaviorStatusType type;
        public float time;

        public AIBehaviorStatus(AIBehaviorStatusType type, float time)
        {
            this.type = type;
            this.time = time;
        }
    }
}
