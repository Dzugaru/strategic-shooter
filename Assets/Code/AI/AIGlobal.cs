﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AI;
using System;

[ExecuteInEditMode]
public class AIGlobal : MonoBehaviour
{
    public NavCustomInfo nav;
    public GameObject player;
    
    [SerializeField]
    List<ScriptableObject> behaviorSettings;

    

    public AIBehaviorFactory behaviorFactory;

    void Awake()
    {
        var behSettings = new Dictionary<string, IBehaviorSettings>();
        foreach(IBehaviorSettings s in behaviorSettings)        
            behSettings.Add(s.GetType().Name, s);

        behaviorFactory = new AIBehaviorFactory(behSettings);        
    }
    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    private void OnValidate()
    {
        Awake();
    }
}
