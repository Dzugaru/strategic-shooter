﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace AI
{
    public class AIBehaviorFactory
    {
        class BehaviorData
        {
            public FreeList freeList;            
        }

        Dictionary<string, IBehaviorSettings> behSettings;
        Dictionary<Type, BehaviorData> behaviors = new Dictionary<Type, BehaviorData>();

        public AIBehaviorFactory(Dictionary<string, IBehaviorSettings> behSettings)
        {
            this.behSettings = behSettings;
        }

        public T Create<T>(IAgent agent) where T : AIBehavior, new()
        {
            BehaviorData bd;
            
            if (!behaviors.TryGetValue(typeof(T), out bd))
            {
                //NOTE: you cannot have a closure here, cause this
                //will lead to allocation even inside if(false),
                //so we move closure to another method
                bd = CreateBehaviorData<T>(agent);
                behaviors.Add(typeof(T), bd);
            }
            var beh = bd.freeList.New<T>();
            beh.agent = agent;
            beh.status = new AIBehaviorStatus(AIBehaviorStatusType.Default, Time.time);
            return beh;
        }

        public IBehaviorSettings GetSettings(string name)
        {
            IBehaviorSettings settings = null;
            behSettings.TryGetValue(name, out settings);
            return settings;
        }

        BehaviorData CreateBehaviorData<T>(IAgent agent) where T : AIBehavior, new()
        {
            string settingsName = typeof(T).Name + "Settings";
            IBehaviorSettings settings = GetSettings(settingsName);
            BehaviorData bd = new BehaviorData()
            {
                freeList = new FreeList(() => new T().Init(this, settings))
            };
            return bd;
        }

        public void Return(IFreeListObject b)
        {
            behaviors[b.GetType()].freeList.Return(b);
        }
    }
}
