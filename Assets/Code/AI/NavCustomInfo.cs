﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;
using AI;

public class NavCustomInfo : ScriptableObject
{
    [Serializable]
    public class Wall
    {
        public Vector3 a, b;

        public Wall(Vector3 a, Vector3 b)
        {
            this.a = a;
            this.b = b;
        }

        public Vector3 center => 0.5f * (a + b);

        public override string ToString()
        {
            return $"{a.ToString()}-{b.ToString()}";
        }
    }


    public List<Wall> walls;   

    public void BakeWalls()
    {
        NavMeshTriangulation triangulatedNavMesh = NavMesh.CalculateTriangulation();
        var verts = triangulatedNavMesh.vertices;
        var indices = triangulatedNavMesh.indices;        

        //Get all wall edges:
        //edges that don't have a corresponding pair in another triangle
        IEnumerable<Wall> TriToWalls(Vector3[] vs)
        {            
            for (int i = 0; i < 3; i++)            
                yield return new Wall(vs[i], vs[(i + 1) % 3]);            
        }

        var allEdges = indices.Select((idx, k) => (idx, k)) //k - position in indices list
                           .GroupBy(x => x.k / 3, x => verts[x.idx]) //group into sublists of len 3 using k
                           .SelectMany(x => TriToWalls(x.ToArray())) //convert each tri to wall edges
                           .ToList(); 

        var wallEdges = new List<Wall>();        
        //O(N^2)
        for (int i = 0; i < allEdges.Count; i++)
        {
            var e1 = allEdges[i];
            bool hasPair = false;            
            for (int j = 0; j < allEdges.Count; j++)
            {
                if (i == j) 
                    continue;
                var e2 = allEdges[j];
                if (e1.a == e2.b && e1.b == e2.a)
                {
                    hasPair = true;
                    break;
                }
            }

            if(!hasPair)            
                wallEdges.Add(e1);            
        }

        walls = wallEdges;
    }
}

