﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using AI;
using UnityEngine.AI;
using System;

public class Agent : MonoBehaviour, IAgent, IDamageable
{
    AIBehavior behavior;
    NavMeshAgent nmAgent;

    public Weapon weapon;

    [NonSerialized]
    public AgentInfo info;

    [NonSerialized]
    public AIGlobal aiGlobal;

    public float hp;
    

    public AgentInfo Info => info;

    public AIGlobal AIGlobal => aiGlobal;

    public Vector3 Position => transform.position;

    public GameObject GameObject => this.gameObject;

    void Awake()
    {
        aiGlobal = SceneManager.GetActiveScene().GetRootGameObjects()
                               .First(x => x.name == "AIGlobal").GetComponent<AIGlobal>();
        nmAgent = GetComponent<NavMeshAgent>(); 
    }

    void Start()
    {
        behavior = aiGlobal.behaviorFactory.Create<Zombie>(this);
    }
    
    void Update()
    {
        //weapon.IsTriggerHeld = Input.GetAxis("Fire1") > 0;        
        //behavior = aiGlobal.behaviorFactory.Create<RunToCover>(this);
        behavior.Do();
        
    }

    private void LateUpdate()
    {
        //behavior.Free();
    }
    

    public void MoveTo(Vector3 pos)
    {
        nmAgent.destination = pos;
    }

    public void Stop()
    {
        nmAgent.isStopped = true;
    }

    public void Damage(float amount)
    {
        GetComponent<Animator>().SetTrigger("Damaged");
        hp = Mathf.Max(0, hp - amount);
        if(hp == 0)
        {
            ((Zombie)behavior).Die();
        }
    }
}
