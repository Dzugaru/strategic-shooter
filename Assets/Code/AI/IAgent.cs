﻿using AI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface IAgent
{
    AgentInfo Info { get; }
    AIGlobal AIGlobal { get; }
    Vector3 Position { get; }
    GameObject GameObject { get; }

    void MoveTo(Vector3 pos);
    void Stop();
}