﻿using AI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "ZombieSettings", menuName = "ScriptableObjects/ZombieSettings", order = 1)]
public class ZombieSettings : ScriptableObject, IBehaviorSettings
{
    public float pathRecalcInterval;
    public float attackStartDistance, attackConnectDistance, attackDamageTime, attackCooldown;
    public float attackTurnSpeed;
    public float dieTime;
}

