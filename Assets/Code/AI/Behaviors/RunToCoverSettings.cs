﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;
using AI;

[CreateAssetMenu(fileName = "RunToCoverSettings", menuName = "ScriptableObjects/RunToCoverSettings", order = 1)]
public class RunToCoverSettings : ScriptableObject, IBehaviorSettings
{
    public float maxSearchDist;

    [Range(0, 1)]
    public float scoreByDist;

    public float rethinkInterval;
}

