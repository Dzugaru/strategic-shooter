﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class RunToCover : AIBehavior, IFreeListObject
    {
        RunToCoverSettings settings;
        Vector3? target;

        public Vector3? Target => target;       
        
        public RunToCover()
        {
            
        }

        public override AIBehavior Init(AIBehaviorFactory factory, IBehaviorSettings settings)
        {
            base.Init(factory, settings);
            this.settings = (RunToCoverSettings)settings;            
            return this;
        }

        public override void InitializeForFreeList()
        {
            target = null;
        }

        public override bool Do()
        {            
            if(status.type == AIBehaviorStatusType.Running && Time.time - status.time < settings.rethinkInterval)            
                return true;            

            if (!target.HasValue)
            {               
                target = FindGoodCover();
                if(!target.HasValue)
                {
                    status = new AIBehaviorStatus(AIBehaviorStatusType.Failed, Time.time);
                    return false;
                }
            }
            
            agent.MoveTo(target.Value);
            status = new AIBehaviorStatus(AIBehaviorStatusType.Running, Time.time);
            return true;            
        }

        #region Find good cover
        //DEBUG public
        public struct ConsideredWall
        {
            public NavCustomInfo.Wall wall;
            public float score;        
            
            public ConsideredWall SetScore(float score)
            {
                return new ConsideredWall() { wall = this.wall, score = score };
            }
        }
        
        //DEBUG public
        public List<ConsideredWall> consideredWalls = new List<ConsideredWall>();

        Vector3? FindGoodCover()
        {
            Vector3 pos = agent.Position;             

            consideredWalls.Clear();
            var walls = agent.AIGlobal.nav.walls;

            //Filter nearby
            foreach(var w in walls)
            {
                var dist = (w.center - pos).magnitude;
                if (dist < settings.maxSearchDist)                
                    consideredWalls.Add(new ConsideredWall() { wall = w, score = 1f });                
            }

            //Score by enemy-facing
            for (int i = 0; i < consideredWalls.Count; i++)
            {
                var w = consideredWalls[i];
                int enemyFacing = 0;
                foreach (var epos in agent.Info.enemyPoss)
                {
                    var normal = (w.wall.b - w.wall.a).ProjectOnGround().OrthogonalRight();
                    if (Vector3.Dot(normal, (epos.position - w.wall.b).ProjectOnGround()) > 0)                    
                        enemyFacing++;                    
                }

                var score = w.score * (1 - (float)enemyFacing / agent.Info.enemyPoss.Count);
                consideredWalls[i] = consideredWalls[i].SetScore(score);
            }

            //Score by distance
            for (int i = 0; i < consideredWalls.Count; i++)
            {
                var w = consideredWalls[i];
                var dist = (w.wall.center - pos).magnitude;
                var score = w.score * Mathf.Lerp(1, 1 - settings.scoreByDist, dist / settings.maxSearchDist);
                consideredWalls[i] = consideredWalls[i].SetScore(score);
            }

            if (consideredWalls.Count == 0)
                return null;
            else
                return consideredWalls.MaxBy(x => x.score).wall.center;
        }
        #endregion
    }
}
