﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace AI
{
    //TODO: refactor this behavior - push Movement, Dying into SubBehs,
    //make Attack track it's own cooldown (don't launch if Do returns false for example)
    public class Zombie : AIBehavior, IFreeListObject
    {
        enum SubState
        {
            None,
            Attacking,
            Dying
        }

        ZombieSettings settings;

        SubState subState;
        AIBehavior subBehavior;
        float lastAttackTime, diedTime;

        public override void InitializeForFreeList()
        {
            subState = SubState.None;
            subBehavior = null;
            lastAttackTime = float.MinValue;
        }

        public override AIBehavior Init(AIBehaviorFactory factory, IBehaviorSettings settings)
        {
            base.Init(factory, settings);
            this.settings = (ZombieSettings)settings;
            return this;
        }

        public override bool Do()
        {
            if (subState == SubState.Dying)
            {
                ProcessDying();
                return true;
            }

            ProcessAttack();
            return RecalcPath();
        }
       
        void ProcessAttack()
        {            
            if (subState == SubState.Attacking ||
                ((agent.Position - agent.AIGlobal.player.transform.position).magnitude <= settings.attackStartDistance &&
                 Time.time - lastAttackTime > settings.attackCooldown))
            {
                if (subBehavior == null)
                    subBehavior = factory.Create<ZombieAttack>(agent);

                subState = SubState.Attacking;
                if (!subBehavior.Do())
                {
                    if (subBehavior.status.type == AIBehaviorStatusType.Success)
                        lastAttackTime = Time.time;
                    subBehavior.Free();
                    subBehavior = null;
                    subState = SubState.None;
                }
            }            
        }

        bool RecalcPath()
        {
            if (status.type == AIBehaviorStatusType.Running && Time.time - status.time < settings.pathRecalcInterval)
                return true;

            status = new AIBehaviorStatus(AIBehaviorStatusType.Running, Time.time);
            agent.MoveTo(agent.AIGlobal.player.transform.position);
            return true;
        }

        public void Die()
        {
            subState = SubState.Dying;
            agent.Stop();
            agent.GameObject.GetComponent<Animator>().SetTrigger("Die");
            diedTime = Time.time;
        }

        void ProcessDying()
        {
            if (Time.time - diedTime >= settings.dieTime)
            {
                this.Free();
                GameObject.Destroy(agent.GameObject);
            }
        }
    }
}
