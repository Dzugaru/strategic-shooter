﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace AI
{
    public class ZombieAttack : AIBehavior, IFreeListObject
    {
        ZombieSettings settings;
        float startTime;

        public override AIBehavior Init(AIBehaviorFactory factory, IBehaviorSettings settings)
        {            
            base.Init(factory, settings);
            //Use root settings instead
            this.settings = (ZombieSettings)factory.GetSettings("ZombieSettings");
            return this;
        }

        public override bool Do()
        {
            TurnToTarget();

            if (status.type == AIBehaviorStatusType.Default)
            {
                startTime = Time.time;
                status = new AIBehaviorStatus(AIBehaviorStatusType.Running, Time.time);
                agent.GameObject.GetComponent<Animator>().SetTrigger("Attack");                
                return true;
            }
            else if(Time.time - status.time >= settings.attackDamageTime)
            {
                if((agent.Position - agent.AIGlobal.player.transform.position).magnitude <= settings.attackConnectDistance)
                {
                    status = new AIBehaviorStatus(AIBehaviorStatusType.Success, Time.time);                   
                }                
                else
                {
                    status = new AIBehaviorStatus(AIBehaviorStatusType.Failed, Time.time);
                }
                return false;
            }
            return true;
        }

        private void TurnToTarget()
        {
            Vector2 targetAngle = (agent.AIGlobal.player.transform.position - agent.Position).ProjectOnGround();
            Vector2 lookAngle = agent.GameObject.transform.forward.ProjectOnGround();
            float angle = Vector2.SignedAngle(lookAngle, targetAngle);            
            float turnAngle = -Mathf.Sign(angle) * Mathf.Min(Mathf.Abs(angle), settings.attackTurnSpeed * Time.deltaTime);            
            Quaternion rot = Quaternion.AngleAxis(turnAngle, Vector3.up);
            agent.GameObject.transform.rotation *= rot;
        }

        public override void InitializeForFreeList()
        {
            
        }
    }
}
