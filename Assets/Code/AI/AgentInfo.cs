﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AI;
using UnityEngine;

namespace AI
{
    public class KnownEnemyPosition
    {
        public Vector3 position;
        public float probability;
    }

    public class AgentInfo
    {
        public List<KnownEnemyPosition> enemyPoss;
    }
}
