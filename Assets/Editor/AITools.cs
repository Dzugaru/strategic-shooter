﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using AI;

public class AITools : EditorWindow
{
    List<Color> randomColors;
    NavCustomInfo navCustomInfo;

    Vector3? targetPoint;
    List<RunToCover.ConsideredWall> consWalls;

    [MenuItem("Tools/AI")]
    static void Init()
    {  
        AITools window = GetWindow<AITools>();
        window.Show();
    }

    void OnGUI()
    {
        if(GUILayout.Button("Bake navigation custom"))
        {
            navCustomInfo = ScriptableObject.CreateInstance<NavCustomInfo>();
            navCustomInfo.BakeWalls();
            AssetDatabase.CreateAsset(navCustomInfo, $"Assets/Scenes/{SceneManager.GetActiveScene().name}/NavCustomInfo.asset");
        }

        if(GUILayout.Button("Test cover"))
        {
            TestCover();
        }
    }

    void Awake()
    {
        randomColors = new List<Color>();
        for (int i = 0; i < 32; i++)        
            randomColors.Add(UnityEngine.Random.ColorHSV(0, 1, 1, 1, 1, 1, 1, 1));
        
        SceneView.duringSceneGui += this.OnSceneGUI;
    }

    void OnDestroy()
    {       
        SceneView.duringSceneGui -= this.OnSceneGUI;
    }

    void OnSceneGUI(SceneView sceneView)
    {
        if (targetPoint.HasValue)
        {
            Handles.color = Color.red;
            Handles.SphereHandleCap(0, targetPoint.Value, Quaternion.identity, 0.1f, EventType.Repaint);
        }

        if (consWalls != null)
        {
            foreach (var wall in consWalls)
            {
                Handles.color = Color.Lerp(Color.red, Color.green, wall.score);
                Handles.DrawAAPolyLine(5f, wall.wall.a, wall.wall.b);
            }
        }

        if (navCustomInfo != null)
        {
            Handles.color = Color.magenta;
            foreach (var wall in navCustomInfo.walls)
                Handles.DrawAAPolyLine(5f, wall.a, wall.b);
        }       
    }

    private void TestCover()
    {
        var rootObjs = SceneManager.GetActiveScene().GetRootGameObjects();
        var aiGlobal = rootObjs.First(x => x.name == "AIGlobal").GetComponent<AIGlobal>();

        var debugObj = rootObjs.First(x => x.name == "DEBUG");
        var agentPos = debugObj.transform.Cast<Transform>().First(x => x.name == "Agent").position;
        var enemiesPoss = new List<Vector3>();
        foreach (Transform t in debugObj.transform.Cast<Transform>().First(x => x.name == "Enemies"))
        {
            if(t.gameObject.activeSelf)
                enemiesPoss.Add(t.position);
        }

        //Debug.Log($"Agentpos {agentPos}");

        AgentInfo info = new AgentInfo()
        {
            enemyPoss = enemiesPoss.Select(x => new KnownEnemyPosition() { position = x, probability = 1f }).ToList()
        };
        TestAgent agent = new TestAgent();
        agent.Position = agentPos;
        agent.AIGlobal = aiGlobal;
        agent.Info = info;

        RunToCover beh = aiGlobal.behaviorFactory.Create<RunToCover>(agent);
        beh.Do();

        consWalls = beh.consideredWalls;

        //Debug.Log(beh.Target);
        //targetPoint = beh.Target;
        //Debug.Log(agentPos);
        //Debug.Log(enemiesPoss.PrettyPrint());        
    }
}

